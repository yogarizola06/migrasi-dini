<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class MenuAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700',
        'menu/vendors/css/vendors.min.css',
        'menu/css/bootstrap.css',
        'menu/css/bootstrap-extended.css',
        'menu/css/colors.css',
        'menu/css/components.css',
        'menu/css/core/menu/menu-types-vertical/vertical-menu.css',
        'menu/css/core/colors/palette-gradient.css',
        'menu/css/plugins/forms/tags/tagging.min.css',
        'menu/css/pages/chat-application.css',
        'menu/vendors/css/extensions/toastr.css',
        'menu/css/plugins/extensions/toastr.css',
        'menu/vendors/css/tables/datatable/datatables.min.css',
        'menu/css/style.css',
        'menu/css/plugins/fontawesome/css/all.min.css',
        'menu/css/pages/email-application.css',
        'menu/vendors/css/forms/toggle/switchery.min.css',
        'menu/css/plugins/forms/switch.min.css',
        'menu/css/core/colors/palette-switch.min.css',
        'menu/vendors/css/forms/toggle/switchery.min.css',
    ];
    public $js = [
        'menu/vendors/js/vendor.js',
        'menu/vendors/js/ui/jquery.sticky.js',
        'menu/vendors/js/extensions/sweetalert2.all.js',
        'menu/js/polyfill.min.js',
        'menu/js/core/app-menu.js',
        'menu/js/core/app.js',
        'menu/js/moment/moment.min.js',
        'menu/js/mqtt.min.js',
        'menu/vendors/js/extensions/toastr.min.js',
        'menu/vendors/js/forms/tags/tagging.min.js',
        'menu/js/apexcharts/dist/apexcharts.js',
        'menu/js/ViewerJS/pdf.worker.js',
        'menu/js/scripts/pages/email-application.js',
        'menu/js/scripts/pages/content-sidebar.js',
        'menu/js/lodash.min.js',
        'menu/js/scripts.js',
        'menu/js/scripts/tables/datatables/datatable-styling.js',
        'menu/vendors/js/tables/datatable/datatables.min.js',
        'menu/vendors/js/ui/jquery.sticky.js',
        'menu/js/yii2-dynamic-form.js',
        'menu/vendors/js/forms/toggle/switchery.min.js',
        'menu/js/scripts/forms/switch.min.js',
        'menu/vendors/js/forms/toggle/switchery.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
        'yii\bootstrap4\BootstrapPluginAsset',
    ];
}