<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AuthAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        "https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700",
        "auth/vendors/css/vendors.min.css",
        "auth/css/bootstrap.css",
        "auth/css/bootstrap-extended.css",
        "auth/css/colors.css",
        "auth/css/components.css",
        "auth/css/core/colors/palette-gradient.css",
        "auth/css/pages/login-register.css",
        "auth/css/style.css",
    ];
    public $js = [
        "vendors/js/vendors.min.js",
        "auth/js/core/app-menu.js",
        "auth/js/core/app.js",
        "auth/vendors/js/ui/jquery.sticky.js",
        "auth/vendors/js/forms/validation/jqBootstrapValidation.js",
        "auth/js/scripts/forms/form-login-register.js",
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
        'yii\bootstrap4\BootstrapPluginAsset',
    ];
}
