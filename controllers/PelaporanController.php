<?php

namespace app\controllers;

use app\models\Pelaporan;
use app\models\PelaporanClass;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PelaporanController implements the CRUD actions for Pelaporan model.
 */
class PelaporanController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Pelaporan models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new PelaporanClass();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pelaporan model.
     * @param int $id_pelaporan Id Pelaporan
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id_pelaporan)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_pelaporan),
        ]);
    }

    /**
     * Creates a new Pelaporan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Pelaporan();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id_pelaporan' => $model->id_pelaporan]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Pelaporan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id_pelaporan Id Pelaporan
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id_pelaporan)
    {
        $model = $this->findModel($id_pelaporan);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_pelaporan' => $model->id_pelaporan]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Pelaporan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id_pelaporan Id Pelaporan
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id_pelaporan)
    {
        $this->findModel($id_pelaporan)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pelaporan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id_pelaporan Id Pelaporan
     * @return Pelaporan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_pelaporan)
    {
        if (($model = Pelaporan::findOne(['id_pelaporan' => $id_pelaporan])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
