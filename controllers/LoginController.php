<?php

namespace app\controllers;

use Yii;
use app\models\LoginForm;

class LoginController extends \yii\web\Controller
{

    public function actionIndex()
    {
        $this->layout = "auth";

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            if (Yii::$app->user->identity->username == "admin") {
                $session = Yii::$app->session;
                $session->open();
                return $this->redirect(['/dashboard/dashboard-admin1']);
            }else if(Yii::$app->user->identity->username == "admin_pelapor"){
                $session = Yii::$app->session;
                $session->open();
                return $this->redirect(['/dashboard/dashboard-admin1']);
            }else{
                $session = Yii::$app->session;
                $session->open();
                return $this->redirect(['/dashboard/karadenim']);
            }
        }

        $model->password = '';
        return $this->render('index', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}