<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;


use app\models\Deteni;
use app\models\DeteniSearch;
use app\models\CommunityHouse;
use app\models\Pelaporan;



/**
 * deteniController implements the CRUD actions for deteni model.
 */
class DeteniController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all deteni models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new DeteniSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);
        $dataProvider->query->andWhere(['deleted_at'=>null])->orderBy('created_at DESC');


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single deteni model.
     * @param int $id_deteni Id deteni
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id_deteni)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_deteni),
        ]);
    }

    /**
     * Creates a new deteni model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Deteni();

        $dataCH = ArrayHelper::map(CommunityHouse::find()->andWhere(['deleted_at'=>null])
        ->all(),'id_community_house','nama_community_house');


        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                $model->tmp_image = UploadedFile::getInstance($model, 'tmp_image');
                $link = $model->foto;
                if ($model->tmp_image != null) {

                    $dir = "foto/foto_deteni";
                    $base_url = Yii::getAlias('@webroot');

                    if (!is_dir($dir)) {
                        FileHelper::createDirectory($dir);
                    }

                    $link = $dir . '/' . strtolower($model->nama_deteni) . '_' . strtotime('now') . '.' . $model->tmp_image->extension;
                    $model->tmp_image->saveAs($link, false);

                }
                $model->tgl_lahir = date("Y-m-d", strtotime($model->tgl_lahir));
                $model->tgl_masuk = date("Y-m-d", strtotime($model->tgl_masuk));
                $model->foto = $link;
                $model->created_at = date("Y-m-d h:i:s");

                if ($model->save(false)) {
                    Yii::$app->session->setFlash('success', 'Deteni Telah di Tambah');
                    return $this->redirect(['create']);
                }else{
                    Yii::$app->session->setFlash('danger', 'Deteni Gagal di Tambah');
                    return $this->redirect(['create']);
                }
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'dataCH' => $dataCH,
        ]);
    }


    /**
     * Updates an existing Sent model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id_deteni)
    {
        $model = $this->findModel($id_deteni);

        $dataCH = ArrayHelper::map(CommunityHouse::find()->andWhere(['deleted_at'=>null])
        ->all(),'id_community_house','nama_community_house');

        date_default_timezone_set("Asia/Jakarta");

        if ($this->request->isPost && $model->load($this->request->post())) {

            $model_deteni = Deteni::find()->where(['id_deteni' => $model->id_deteni])->one();
            $model->tmp_image = UploadedFile::getInstance($model, 'tmp_image');
            $link = $model->foto;

            if ($model->tmp_image != null) {

                $dir = "foto/foto_deteni";
                $base_url = Yii::getAlias('@webroot');

                if (!is_dir($dir)) {
                    FileHelper::createDirectory($dir);
                }
                $isFile = Yii::getAlias('@webroot') . '/' . $model_deteni->foto;
                if (is_file($isFile)) {
                    unlink($model_deteni->foto);
                }

                $link = $dir . '/' . strtolower($model->nama_deteni) . '_' . strtotime('now') . '.' . $model->tmp_image->extension;
                $model->tmp_image->saveAs($link, false);

            }

            $model->foto = $link;
            $model->updated_at = date("Y-m-d h:i:s");

            if ($model->save(false)) {
                Yii::$app->session->setFlash('success', 'Data Deteni Telah di Ubah');
                return $this->redirect(['index']);
            }else{
                Yii::$app->session->setFlash('danger', 'Maaf Data Deteni Gagal di Ubah');
                return $this->redirect(['update']);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'dataCH' => $dataCH,
        ]);
    }

    /**
     * Reporting an existing deteni model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id_deteni Id deteni
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionReport($id_deteni)
    {
        $model = $this->findModel($id_deteni);
        $modelPelaporan = new Pelaporan();

        if ($this->request->isPost && $model->load($this->request->post())) {

            $model_deteni = Deteni::find()->where(['id_deteni' => $model->id_deteni])->one();
            $model->tmp_image = UploadedFile::getInstance($model, 'tmp_image');
            $link = $model->foto;

            if ($model->tmp_image != null) {

                $dir = "foto/foto_deteni";
                $base_url = Yii::getAlias('@webroot');

                if (!is_dir($dir)) {
                    FileHelper::createDirectory($dir);
                }
                $isFile = Yii::getAlias('@webroot') . '/' . $model_deteni->foto;
                if (is_file($isFile)) {
                    unlink($model_deteni->foto);
                }

                $link = $dir . '/' . strtolower($model->nama_deteni) . '_' . strtotime('now') . '.' . $model->tmp_image->extension;
                $model->tmp_image->saveAs($link, false);

            }

            $model->id_pengungsi = $model->id_deteni;
            $model->id_community_house = $model->id_community_house;
            $model->foto_terbaru = $link;
            $model->created_at = date("Y-m-d h:i:s");
        }

            if ($model->save(false)) {
                Yii::$app->session->setFlash('success', 'Data Deteni Telah di Diperbarui');
                return $this->redirect(['index']);
            }else{
                Yii::$app->session->setFlash('danger', 'Maaf Data Deteni Gagal di Diperbarui');
                return $this->redirect(['update']);
            }

        return $this->renderAjax('report',[
            'model' => $model,
        ]);
    }


    /**
     * Deletes an existing deteni model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id_deteni Id deteni
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id_deteni)
    {
        $this->findModel($id_deteni)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Remove an existing Deteni model.
     * If remove is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionRemove($id_deteni)
    {
        date_default_timezone_set("Asia/Jakarta");

        $model = $this->findModel($id_deteni);
        $data = Yii::$app->request->post();
        $model->deleted_at = date("Y-m-d h:i:s");

        if ($model->save(false)) {
            $res = [
                'title' => 'Hapus Deteni',
                'message' => 'Penghapusan Deteni Berhasil',
                'type' => 'success',
            ];
        } else {
            $res = [
                'title' => 'Hapus Deteni',
                'message' => 'Gagal Menghapus Deteni',
                'type' => 'error',
            ];
        }

        return Json::encode($res);
    }


    /**
     * Finds the deteni model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id_deteni Id deteni
     * @return deteni the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_deteni)
    {
        if (($model = deteni::findOne(['id_deteni' => $id_deteni])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
