<?php

namespace app\controllers;

use Yii;
use app\models\CommunityHouse;
use app\models\Pengguna;
use app\models\PenggunaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * PenggunaController implements the CRUD actions for Pengguna model.
 */
class PenggunaController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Pengguna models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new PenggunaSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pengguna model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Pengguna model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Pengguna();

        $dataCH = ArrayHelper::map(CommunityHouse::find()->andWhere(['deleted_at'=>null])
        ->all(),'id_community_house','nama_community_house');

        if ($model->load(Yii::$app->request->post())) {
            $model->password = Yii::$app->getSecurity()->generatePasswordHash($model->repeat_password);
            $model->access_token = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MzIsIm5hbWUiOiJ5b2dhcml6b2xhMDYxIiwicGhvbmVfbnVtYmVyIjpudWxsLCJlbWFpbCI6InlvZ2FyaXpvbGEwNkBnbWFpMjFsMi5jb20iLCJ1c2VybmFtZSI6InlvZ2FyaXpvbGEwNiIsInBhc3N3b3JkIjoiJDJ5JDEzJEl4MkhBWHZkaVBVOUZUbmtnUmJGNi5rRWVYem82Q1';

            if($model->save(false)){
                return $this->redirect(['index']);
            }else{
                print_r('Gagal simpan');
                exit;
            }
        }

        return $this->render('create', [
            'model' => $model,
            'dataCH' => $dataCH,
        ]);
    }

    /**
     * Updates an existing Pengguna model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $dataCH = ArrayHelper::map(CommunityHouse::find()->andWhere(['deleted_at'=>null])
        ->all(),'id_community_house','nama_community_house');

        if ($model->load(Yii::$app->request->post())) {
            $model->password = Yii::$app->getSecurity()->generatePasswordHash($model->repeat_password);

            if($model->save(false)){
                return $this->redirect(['index']);
            }else{
                print_r('Gagal simpan');
                exit;
            }
        }

        return $this->render('update', [
            'model' => $model,
            'dataCH' => $dataCH,
        ]);
    }

    /**
     * Deletes an existing Pengguna model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pengguna model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Pengguna the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pengguna::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
