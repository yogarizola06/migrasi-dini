<?php

namespace app\controllers;

use Yii;
use app\models\CommunityHouse;
use app\models\CommunityHouseSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * CommunityHouseController implements the CRUD actions for CommunityHouse model.
 */
class CommunityHouseController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all CommunityHouse models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new CommunityHouseSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);
        $dataProvider->query->andWhere(['deleted_at'=>null])->orderBy('created_at DESC');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CommunityHouse model.
     * @param int $id_community_house Id Community House
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id_community_house)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_community_house),
        ]);
    }

    /**
     * Creates a new CommunityHouse model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new CommunityHouse();

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                $model->created_at = date("Y-m-d h:i:s");
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Community House Telah di Tambah');
                    return $this->redirect(['create']);
                }else{
                    Yii::$app->session->setFlash('danger', 'Community House Gagal di Tambah');
                    return $this->redirect(['create']);
                }
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CommunityHouse model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id_community_house Id Community House
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id_community_house)
    {
        $model = $this->findModel($id_community_house);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Remove an existing Community House model.
     * If remove is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionRemove($id_community_house)
    {
        date_default_timezone_set("Asia/Jakarta");

        $model = $this->findModel($id_community_house);
        $model->deleted_at = date("Y-m-d h:i:s");

        if ($model->save(false)) {
            $res = [
                'title' => 'Hapus Community House',
                'message' => 'Penghapusan Community House Berhasil',
                'type' => 'success',
            ];
        } else {
            $res = [
                'title' => 'Hapus Community House',
                'message' => 'Gagal Menghapus Community House',
                'type' => 'error',
            ];
        }

        return Json::encode($res);
    }

    /**
     * Deletes an existing CommunityHouse model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id_community_house Id Community House
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id_community_house)
    {
        $this->findModel($id_community_house)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CommunityHouse model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id_community_house Id Community House
     * @return CommunityHouse the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_community_house)
    {
        if (($model = CommunityHouse::findOne(['id_community_house' => $id_community_house])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
