<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

class DashboardController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionDashboardAdmin1()
    {
        $this->layout = "main_menu";
        if (Yii::$app->user->isGuest) 
        {
            return $this->redirect(['/login']);
        }else{
            return $this->render('dashboard_penampungan');
        }
    }

    public function actionDashboardAdmin2()
    {
        $this->layout = "main_menu";
        if (Yii::$app->user->isGuest) 
        {
            return $this->redirect(['/login']);
        }else{
            return $this->render('dashboard_pelapor');
        }
    }

    public function actionKaradenim()
    {
        $this->layout = "main_menu";
        if (Yii::$app->user->isGuest) 
        {
            return $this->redirect(['/login']);
        }else{
            return $this->render('karadenim');
        }
    }
}
