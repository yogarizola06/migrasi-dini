const totalAttendance = (url, callback) => {
	$.ajax({
		url: url,
		method: 'GET'
	})
		.done(function(response) {
			let parse = JSON.parse(response);
			if (parse.code == 200) {
				callback(parse);
			} else {
				console.warn(parse);
			}
		})
		.fail(function(error) {
			console.error(error);
		});
};
