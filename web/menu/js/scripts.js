// (function(window, undefined) {
// 	'use strict';

// 	/*
//   NOTE:
//   ------
//   PLACE HERE YOUR OWN JAVASCRIPT CODE IF NEEDED
//   WE WILL RELEASE FUTURE UPDATES SO IN ORDER TO NOT OVERWRITE YOUR JAVASCRIPT CODE PLEASE CONSIDER WRITING YOUR SCRIPT HERE.  */

// })(window);
const loadModal = (url) => {
	$('#modalContent').html('');
	$('#modal')
		.appendTo('body')
		.modal({
			show: true,
			tabindex: false,
			backdrop: 'static',
			keyboard: 'false'
		})
		.find('#modalContent')
		.load(url);
};

const convertToCSV = (objArray) => {
	var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
	var str = '';

	for (var i = 0; i < array.length; i++) {
		var line = '';
		for (var index in array[i]) {
			if (line != '') line += ',';

			line += array[i][index];
		}
		str += line + '\r\n';
	}
	return str;
};

const exportCSVFile = (headers, items, fileTitle) => {
	if (headers) {
		items.unshift(headers);
	}

	// Convert Object to JSON
	var jsonObject = JSON.stringify(items);

	var csv = convertToCSV(jsonObject);

	var exportedFilenmae = fileTitle + '.csv' || 'export.csv';

	var blob = new Blob([ csv ], {
		type: 'text/csv;charset=utf-8;'
	});
	if (navigator.msSaveBlob) {
		// IE 10+
		navigator.msSaveBlob(blob, exportedFilenmae);
	} else {
		var link = document.createElement('a');
		if (link.download !== undefined) {
			// feature detection
			// Browsers that support HTML5 download attribute
			var url = URL.createObjectURL(blob);
			link.setAttribute('href', url);
			link.setAttribute('download', exportedFilenmae);
			link.style.visibility = 'hidden';
			document.body.appendChild(link);
			link.click();
			document.body.removeChild(link);
		}
	}
};

const ajaxQrGenerator = (target, targetBase, callback) => {
	swal({
		title: 'Generate QR Code',
		text: 'Are you sure to generate qr code for this data ?',
		type: 'warning',
		buttons: true,
		showCancelButton: true,
		confirmButtonText: 'Generate',
		showLoaderOnConfirm: true,
		preConfirm: () => {
			return fetch(`${target}`)
				.then((response) => {
					if (!response.ok) {
						swal('Failed', response.statusText, 'error');
					}
					return response.json();
				})
				.catch((error) => {
					Swal.showValidationMessage(`Request failed: ${error}`);
				});
		},
		allowOutsideClick: () => !Swal.isLoading()
	}).then((result) => {
		if (!result.dismiss && result.value.code == 200) {
			swal(result.value.title, result.value.message, result.value.type).then((redirected) => {
				if (typeof callback === 'function') {
					callback(result.value.url);
				}
				window.open(targetBase + '/' + result.value.url, '_blank');
			});
		} else {
			swal('Cancelled', 'Cancelled action', 'info');
		}
	});
};

const ajaxDeleteAlert = (target, gridContainer, cb) => {
	swal({
		title: 'Delete Confirmation.',
		text: 'Are you sure you want to delete this item?',
		type: 'warning',
		buttons: true,
		showCancelButton: true,
		confirmButtonText: 'Yes! Delete.',
		confirmButtonColor: '#fa626b',
		showLoaderOnConfirm: true,
		preConfirm: (response) => {
			return fetch(`${target}`)
				.then((response) => {
					if (!response.ok) {
						swal('Failed', response.statusText, 'error');
					}
					return response.json();
				})
				.catch((error) => {
					Swal.showValidationMessage(`Request failed: ${error}`);
				});
		},
		allowOutsideClick: () => !Swal.isLoading()
	}).then((result) => {
		if (!result.dismiss && result.value.code == 200) {
			swal(result.value.title, result.value.message, result.value.type).then(() => {
				if (typeof cb === 'function') {
					cb('done');
				} else {
					$.pjax.reload({
						container: gridContainer
					});
				}
			});
		} else {
			swal({
				title: result.value !== undefined ? result.value.title : 'Cancelled',
				text: result.value !== undefined ? result.value.message : 'Cancelled action',
				type: result.value !== undefined ? result.value.type : 'info'
			});
		}
	});
};

const ajaxTotal = (url, el, opt) => {
	$.get(url, opt)
		.done(function(data) {
			$(el).html(data);
		})
		.fail(function() {
			$(el).html(
				'<button id="' +
					el.replace('#', '') +
					'-load" class="btn-reload btn btn-outline-danger round"><i class="fas fa-redo-alt"></i></button>'
			);
			$(el + '-load').click(function() {
				ajaxTotal(url, el);
			});
		});
};
