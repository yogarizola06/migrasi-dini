function displayLocation(latitude,longitude){
    var request = new XMLHttpRequest();

    var method = 'GET';
    var url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='+latitude+','+longitude+'&sensor=true&key=AIzaSyBRAnJsQlQEKOgsCNfp_bmBKWsBbN1fEu0';
    var async = true;

    request.open(method, url, async);
    request.onreadystatechange = function(){
      if(request.readyState == 4 && request.status == 200){
        var data = JSON.parse(request.responseText);
        var address = data.results[3];
        $("#location").html(address.formatted_address)
      }
    };
    request.send();
};

var successCallback = function(position){
    var x = position.coords.latitude;
    var y = position.coords.longitude;
    displayLocation(x,y);
};

var errorCallback = function(error){
var errorMessage = 'Unknown error';
switch(error.code) {
    case 1:
    errorMessage = 'Permission denied';
    break;
    case 2:
    errorMessage = 'Position unavailable';
    break;
    case 3:
    errorMessage = 'Timeout';
    break;
}
console.log(errorMessage)
};

var options = {
    enableHighAccuracy: true,
    timeout: 1000,
    maximumAge: 0
};

navigator.geolocation.getCurrentPosition(successCallback,errorCallback,options);

$(document).ready(function() {
    var interval = setInterval(function() {
        var momentNow = moment();
        $('#time').html(momentNow.format('YYYY MMMM DD') + ' '+ momentNow.format('A hh:mm:ss'));
    }, 100);
});