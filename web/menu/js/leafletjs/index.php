
<!DOCTYPE html>
<html>
<head>
    <title>Leaflet.draw vector editing handlers</title>

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.4.0.min.js"></script>
    <script src="leaflet/leaflet-src.js"></script>
    <link rel="stylesheet" href="leaflet/leaflet.css"/>
    <script src="Proj4Leaflet/lib/proj4-compressed.js"></script>
    <script src="Proj4Leaflet/src/proj4leaflet.js"></script>


    <script src="src/Leaflet.draw.js"></script>
    <script src="src/Leaflet.Draw.Event.js"></script>
    <link rel="stylesheet" href="src/leaflet.draw.css"/>

    <script src="src/Toolbar.js"></script>
    <script src="src/Tooltip.js"></script>

    <script src="src/ext/GeometryUtil.js"></script>
    <script src="src/ext/LatLngUtil.js"></script>
    <script src="src/ext/LineUtil.Intersect.js"></script>
    <script src="src/ext/Polygon.Intersect.js"></script>
    <script src="src/ext/Polyline.Intersect.js"></script>
    <script src="src/ext/TouchEvents.js"></script>

    <script src="src/draw/DrawToolbar.js"></script>
    <script src="src/draw/handler/Draw.Feature.js"></script>
    <script src="src/draw/handler/Draw.SimpleShape.js"></script>
    <script src="src/draw/handler/Draw.Polyline.js"></script>
    <script src="src/draw/handler/Draw.Marker.js"></script>
    <script src="src/draw/handler/Draw.Circle.js"></script>
    <script src="src/draw/handler/Draw.CircleMarker.js"></script>
    <script src="src/draw/handler/Draw.Polygon.js"></script>
    <script src="src/draw/handler/Draw.Rectangle.js"></script>


    <script src="src/edit/EditToolbar.js"></script>
    <script src="src/edit/handler/EditToolbar.Edit.js"></script>
    <script src="src/edit/handler/EditToolbar.Delete.js"></script>

    <script src="src/Control.Draw.js"></script>

    <script src="src/edit/handler/Edit.Poly.js"></script>
    <script src="src/edit/handler/Edit.SimpleShape.js"></script>
    <script src="src/edit/handler/Edit.Rectangle.js"></script>
    <script src="src/edit/handler/Edit.Marker.js"></script>
    <script src="src/edit/handler/Edit.CircleMarker.js"></script>
    <script src="src/edit/handler/Edit.Circle.js"></script>
</head>
<body>
<div id="map" style="width: 100%; height: 600px; border: 1px solid #ccc"></div>
<textarea id="json" cols="10" style="width: 100%"></textarea>
    <script type="text/javascript" src="json.js"></script>
<script>

    var tmpImg = new Image(); 
    var orgWidth;
    var orgHeight;
    tmpImg.src="https://allhdwallpapers.com/wp-content/uploads/2019/01/Blue-Mosque-Istanbul-Turkey-4K-Wallpapers2.jpg";
    $(tmpImg).on('load',function(){

        orgWidth = tmpImg.width;
        orgHeight = tmpImg.height;
    
        var osmUrl= tmpImg.src, h = orgHeight, w = orgWidth;
        var map = L.map('map', {
            minZoom: 0,
            maxZoom: 4,
            center: [0, -orgHeight],
            zoom: 3,
            crs: L.CRS.Simple
        });

        drawnItems = L.featureGroup().addTo(map);
        var layerControl = new L.Control.Layers({}, {});

        var southWest =  map.unproject([0, h], map.getMaxZoom());
        var northEast = map.unproject([w, 0], map.getMaxZoom());
        var bounds = new L.LatLngBounds(southWest, northEast);
          
        L.imageOverlay(osmUrl, bounds).addTo(map);
        map.setMaxBounds(bounds);

        L.geoJSON(json).addTo(map);

        map.addControl(new L.Control.Draw({
            edit: {
                featureGroup: drawnItems,
                poly: {
                    allowIntersection: false
                }
            },
            draw: {
                polygon: {
                    allowIntersection: true,
                    showArea: true,
                    icon: new L.DivIcon({
                        iconSize: new L.Point(5, 5),
                        className: 'leaflet-div-icon leaflet-editing-icon'
                    }),
                    shapeOptions: {
                        stroke: true,
                        color: '#3388ff',
                        weight: 2,
                    },
                },
                polyline : {
                    icon: new L.DivIcon({
                        iconSize: new L.Point(5, 5),
                        className: 'leaflet-div-icon leaflet-editing-icon'
                    }),
                    shapeOptions: {
                        stroke: true,
                        color: '#3388ff',
                        weight: 2,
                        opacity: 1,
                        fill: false,
                        clickable: true
                    },
                },
                rectangle : {
                     icon: new L.DivIcon({
                        iconSize: new L.Point(5, 5),
                        className: 'leaflet-div-icon leaflet-editing-icon'
                    }),
                    shapeOptions: {
                        stroke: true,
                        color: '#3388ff',
                        weight: 2,
                    },
                },
                circle : {
                     icon: new L.DivIcon({
                        iconSize: new L.Point(5, 5),
                        className: 'leaflet-div-icon leaflet-editing-icon'
                    }),
                    shapeOptions: {
                        stroke: true,
                        color: '#3388ff',
                        weight: 2,
                    },
                }
            }
        }));

        map.on(L.Draw.Event.CREATED, function (e) {
            var layer = e.layer,type = e.layerType;
            var down ;
            if(type === "polygon" ){
               var geojson = {};
               // var seeArea = L.GeometryUtil.geodesicArea(layer.getLatLngs());
               geojson['type'] = 'Feature';
               geojson['properties'] = {};
               geojson['geometry'] = {};
               geojson['geometry']['type'] = "Polygon";
               // // export the coordinates from the layer
               coordinates = [];
               latlngs = layer.getLatLngs();
               $.each(latlngs,function(k,v){
                   for (var i = 0; i < v.length; i++) {
                       coordinates.push([v[i].lng, v[i].lat])
                   }
               })
               // // push the coordinates to the json geometry
               geojson['geometry']['coordinates'] = [coordinates];
               down = JSON.stringify(geojson);
            }else if(type === "rectangle" ){
               var geojson = {};
               geojson['type'] = 'Feature';
               geojson['properties'] = {};
               geojson['geometry'] = {};
               geojson['geometry']['type'] = "Rectangle";
               // export the coordinates from the layer
               coordinates = [];
               latlngs = layer.getLatLngs();
               $.each(latlngs,function(k,v){
                   for (var i = 0; i < v.length; i++) {
                       coordinates.push([v[i].lng, v[i].lat])
                   }
               })
               // push the coordinates to the json geometry
               geojson['geometry']['coordinates'] = [coordinates];
               down = JSON.stringify(geojson);
            }
            $("#json").val(down);
            drawnItems.addLayer(layer);
        });

    });

    function onEachFeature(feature, layer) {
        var popupContent = "<p>I started out as a GeoJSON " +
                feature.geometry.type + ", but now I'm a Leaflet vector!</p>";

        if (feature.properties && feature.properties.popupContent) {
            popupContent += feature.properties.popupContent;
        }

        layer.bindPopup(popupContent);
    }
</script>
</body>
</html>
