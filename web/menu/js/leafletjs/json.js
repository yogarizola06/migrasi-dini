var json = {
	"type": "FeatureCollection",
	"features": [{
		"type": "Feature",
		"properties": {},
		"geometry": {
			"type": "Polygon",
			"coordinates": [
				[
					[6.75, -24.25],
					[6.75, -14.875],
					[18, -14.875],
					[18, -24.25]
				]
			]
		}
	}, {
		"type": "Feature",
		"properties": {},
		"geometry": {
			"type": "Polygon",
			"coordinates": [
				[
					[80.25, -24.5],
					[80.25, -12.75],
					[102.625, -12.75],
					[102.625, -24.5]
				]
			]
		}
	}, {
		"type": "Feature",
		"properties": {},
		"geometry": {
			"type": "Polygon",
			"coordinates": [
				[
					[5, -66.375],
					[7.5, -62.75],
					[7.625, -61.125],
					[11.75, -60.125],
					[16, -60.625],
					[16.5, -62.5],
					[17.875, -65.5],
					[14.625, -67.5],
					[8.25, -68],
					[5.25, -69.5]
				]
			]
		}
	}, {
		"type": "Feature",
		"properties": {},
		"geometry": {
			"type": "Polygon",
			"coordinates": [
				[
					[42.25, -42],
					[42.25, -37.75],
					[54.75, -37.75],
					[54.75, -42]
				]
			]
		}
	}]
}

var freeBus = {
    "type": "FeatureCollection",
    "features": [
        {
            "type": "Feature",
            "geometry": {
                "type": "LineString",
                "coordinates": [
                   [6.75, -24.25],
					[6.75, -14.875],
                ]
            },
            "properties": {
                "popupContent": "This is a free bus line that will take you across downtown.",
                "underConstruction": false
            },
            "id": 1
        },
        {
            "type": "Feature",
            "geometry": {
                "type": "LineString",
                "coordinates": [
                    [7.5, -62.75],
					[7.625, -61.125],
                ]
            },
            "properties": {
                "popupContent": "This is a free bus line that will take you across downtown.",
                "underConstruction": true
            },
            "id": 2
        },
        {
            "type": "Feature",
            "geometry": {
                "type": "LineString",
                "coordinates": [
                    [42.25, -42],
					[42.25, -37.75],
					[54.75, -37.75],
					[54.75, -42]
                ]
            },
            "properties": {
                "popupContent": "This is a free bus line that will take you across downtown.",
                "underConstruction": false
            },
            "id": 3
        }
    ]
};