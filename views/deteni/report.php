<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\deteni */
/* @var $form yii\widgets\ActiveForm */
?>

<section id="base-style">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h1>Form Perbarui Deteni</h1>
                </div>

                <div class="card-content collapse show">
                    <div class="card-body card-dashboard">
                        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
                                <?= $form->field($model, 'tmp_image')->widget(FileInput::classname(), [
                                    'options' => ['accept' => 'image', 'multiple' => false],
                                    'pluginOptions' => [
                                        'fileActionSettings' => [
                                            'removeClass' => 'kv-file-remove btn btn-sm btn-kv btn-danger ',
                                            'zoomClass' => 'kv-file-zoom btn btn-sm btn-kv btn-info ',
                                        ],
                                        'showDrag'         => true,
                                        'showPreview'      => true,
                                        'showCaption'      => false,
                                        'showRemove'       => true,
                                        'showCancel'       => false,
                                        'showUpload'       => false,
                                        'browseClass' => 'btn btn-info btn-sm',
                                        'removeClass' => 'btn btn-danger btn-sm',
                                        'browseIcon' => '<i class="fas fa-folder"></i> ',
                                        'removeIcon' => '<i class="fas fa-trash"></i> ',
                                        'overwriteInitial' => true
                                    ]
                                ])
                                ?>
                        <div class="form-actions right">
                            <a class="btn  btn-danger "href="<?= Url::to(['index']) ?>">Kembali</a>
                            <?= Html::submitButton(Yii::t('app', 'Simpan'), ['class' => 'btn btn-success']) ?>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
