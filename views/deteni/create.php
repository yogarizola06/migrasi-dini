<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\deteni */

$this->title = 'Tambah Deteni';
$this->params['breadcrumbs'][] = ['label' => 'detenis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="deteni-create">

    <?= $this->render('_form', [
        'model' => $model,
        'dataCH' => $dataCH,
    ]) ?>

</div>
