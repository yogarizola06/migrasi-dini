<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\file\FileInput;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\deteni */
/* @var $form yii\widgets\ActiveForm */
?>

<section id="base-style">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h1>Form Deteni</h1>
                </div>

                <?php if(Yii::$app->session->hasFlash('success')): ?>
                    <div class="alert alert-success" role="alert">
                        <?= Yii::$app->session->getFlash('success'); ?>
                    </div>
                <?php endif; ?>

                <?php if(Yii::$app->session->hasFlash('danger')): ?>
                    <div class="alert alert-danger" role="alert">
                        <?= Yii::$app->session->getFlash('danger'); ?>
                    </div>
                <?php endif; ?>

                <div class="card-content collapse show">
                    <div class="card-body card-dashboard">
                        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
                        <div class="row">
                            <div class="col-6">
                                <?= $form->field($model, 'nama_deteni')->textInput(['maxlength' => true]) ?>

                                <?= $form->field($model, 'jk')
                                    ->dropDownList(
                                        ['l' => 'Laki-laki', 'p' => 'Perempuan'],
                                        ['prompt'=>'Pilih Jenis Kelamin']);
                                ?>

                                <?= $form->field($model, 'tempat_lahir')->textInput(['maxlength' => true]) ?>

                                <?= $form->field($model, 'tgl_lahir')->widget(DatePicker::classname(['value' => $model->tgl_lahir, 'type' => DatePicker::TYPE_COMPONENT_PREPEND]), [
                                    'options' => ['placeholder' => 'Masukkan Tanggal Lahir'],
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                    ]
                                ]); ?>

                                <?= $form->field($model, 'kewarganegaraan')->textInput(['maxlength' => true]) ?>

                                <?= $form->field($model, 'ket')->textInput(['maxlength' => true]) ?>

                            </div>
                            <div class="col-6">
                                <?= $form->field($model, 'id_community_house')->widget(Select2::classname(), [
                                    'data' => $dataCH,
                                    'options' => ['placeholder' => 'Pilih Community House'],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ])->label('Community House'); ?>

                                <?= $form->field($model, 'no_unhc')->textInput(['maxlength' => true]) ?>

                                <?= $form->field($model, 'tgl_masuk')->widget(DatePicker::classname(['value' => $model->tgl_masuk, 'type' => DatePicker::TYPE_COMPONENT_PREPEND]), [
                                    'options' => ['placeholder' => 'Masukkan Tanggal Masuk'],
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                    ]
                                ]); ?>

                                <?= $form->field($model, 'tmp_image')->widget(FileInput::classname(), [
                                    'options' => ['accept' => 'image', 'multiple' => false],
                                    'pluginOptions' => [
                                        'initialPreview'   => [
                                            $model->foto != null ? Html::img(Url::base() . '/' . $model->foto, ['class' => 'file-preview-image', 'style' => 'width:50%;', 'alt' => 'Building Images', 'title' => 'Building Image']) : null,
                                        ],
                                        'fileActionSettings' => [
                                            'removeClass' => 'kv-file-remove btn btn-sm btn-kv btn-danger ',
                                            'zoomClass' => 'kv-file-zoom btn btn-sm btn-kv btn-info ',
                                        ],
                                        'showDrag'         => true,
                                        'showPreview'      => true,
                                        'showCaption'      => false,
                                        'showRemove'       => true,
                                        'showCancel'       => false,
                                        'showUpload'       => false,
                                        'browseClass' => 'btn btn-info btn-sm',
                                        'removeClass' => 'btn btn-danger btn-sm',
                                        'browseIcon' => '<i class="fas fa-folder"></i> ',
                                        'removeIcon' => '<i class="fas fa-trash"></i> ',
                                        'overwriteInitial' => true
                                    ]
                                ])
                                ?>
                            </div>
                        </div>
                        <div class="form-actions right">
                            <a class="btn  btn-danger "href="<?= Url::to(['index']) ?>">Kembali</a>
                            <?= Html::submitButton(Yii::t('app', 'Simpan'), ['class' => 'btn btn-success']) ?>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
