<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap4\Modal;
use richardfan\widget\JSRegister;
/* @var $this yii\web\View */
/* @var $searchModel app\models\SentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Deteni';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inbox-index">
    <div class="row">
        <div class="col-xl-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h3>Deteni
                    <a href="<?= Url::to(['create']) ?>" class="pull-right btn btn-sm btn-success">Tambah Data Deteni</a></h3>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12 table-responsive">
                                <?= GridView::widget([
                                    'dataProvider' => $dataProvider,
                                    'filterModel' => $searchModel,
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn'],
                                        'nama_deteni',
                                        'tempat_lahir',
                                        'kewarganegaraan',
                                        'no_unhc',
                                        'ket',
                                        [
                                            'class' => 'yii\grid\ActionColumn',
                                            'template' => '<div class="btn-group">{report}{update}{remove}</div>',
                                            'buttons' => [
                                                'report' => function ($url,$model) {
                                                    return Html::a(
                                                        '<span class="fa fa-eye"></span>' ,'#', [
                                                            'class'=>'btn btn-sm btn-info report',
                                                            'data-id'=>$model->id_deteni,
                                                            'data-toggle'=>'tooltip',
                                                            'data-placement'=>'left',
                                                            'title'=>'Klik disini untuk edit deteni',
                                                        ]);
                                                },
                                                'update' => function ($url,$model) {
                                                    return Html::a(
                                                        '<span class="fa fa-edit"></span>' ,Url::to(['update', 'id_deteni' => $model->id_deteni]), [
                                                            'class'=>'btn btn-sm btn-warning',
                                                            'data-id'=>$model->id_deteni,
                                                            'data-toggle'=>'tooltip',
                                                            'data-placement'=>'left',
                                                            'title'=>'Klik disini untuk edit deteni',
                                                        ]);
                                                },
                                                'remove' => function ($url,$model) {
                                                    return Html::button(Yii::t('app', '<span class="fa fa-trash"></span>'),
                                                    [
                                                        'class' => 'btn btn-sm btn-danger remove_deteni',
                                                        'data-toggle'=>'tooltip',
                                                        'data-id' => $model->id_deteni,
                                                        'data-placement'=>'top', 
                                                        'title'=>'Klik disini untuk hapus deteni',
                                                    ]);
                                                },
                                            ],
                                        ],
                                    ],
                                ]); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php
Modal::begin([
    'id'=>'modal',
    'title'=>'',
    'options' => ['tabindex' => false],
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => false],
    'size'=>'modal-lg',
]);
echo "<div id='modalContent'></div>";
Modal::end();
?>
<?php JSRegister::begin() ?>
<script>

    $(".remove_deteni").click(function(e) {
        remove($(this).data("id"))
    });

    $(".report").click(function(e) {
        report($(this).data('id'));

        function report(id) {
            $('#modal').appendTo("body").modal({
                'show': true,
                'tabindex': false,
                'backdrop': 'static',
                'keyboard': 'false',
            }).find('#modalContent').load("<?= Url::to(['report']) ?>?id_deteni=" + id);
        }
    });

    function remove(id) {
        swal({
            title: "Konfirmasi Hapus!",
            text: "Apakah anda yakin ingin menghapus data deteni ?",
            icon: "warning",
            buttons: true,
            showCancelButton: true,
            dangerMode: true,

        }).then((res) => {
            console.log(res);
            if (res.value) {
                $.ajax({
                    url: "<?= Url::to(['remove']) ?>?id_deteni=" + id,
                    type: "POST",
                    contentType: false,
                    processData: false,
                    success: function(data_res) {
                        var data = JSON.parse(data_res);
                        swal({
                            title: data.title,
                            text: data.message,
                            icon: data.type
                        }).then((result) => {
                            location.href = "<?= Url::to(['index']) ?>";
                        });
                    }
                })

            } else {
                swal("Batal", "Aksi dibatalkan", "success");
            }
        });
    }

</script>
<?php JSRegister::end() ?>