<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\deteni */

$this->title = $model->id_deteni;
$this->params['breadcrumbs'][] = ['label' => 'detenis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="deteni-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id_deteni' => $model->id_deteni], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id_deteni' => $model->id_deteni], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_deteni',
            'nama_deteni',
            'jk',
            'tempat_lahir',
            'tgl_lahir',
            'kewarganegaraan',
            'no_unhc',
            'tgl_masuk',
            'foto',
            'ket',
            'id_community_house',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
