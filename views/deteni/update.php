<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\deteni */

$this->title = 'Ubah deteni: ' . $model->nama_deteni;
$this->params['breadcrumbs'][] = ['label' => 'Detenis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama_deteni, 'url' => ['view', 'id_deteni' => $model->id_deteni]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="deteni-update">

    <?= $this->render('_form', [
        'model' => $model,
        'dataCH' => $dataCH,
    ]) ?>

</div>
