<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Pelaporan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pelaporan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_pengungsi')->textInput() ?>

    <?= $form->field($model, 'id_community_house')->textInput() ?>

    <?= $form->field($model, 'foto_terbaru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'periode_lapor')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
