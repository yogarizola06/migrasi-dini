<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pelaporan */

$this->title = 'Update Pelaporan: ' . $model->id_pelaporan;
$this->params['breadcrumbs'][] = ['label' => 'Pelaporans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_pelaporan, 'url' => ['view', 'id_pelaporan' => $model->id_pelaporan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pelaporan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
