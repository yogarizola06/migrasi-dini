<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pelaporan */

$this->title = 'Create Pelaporan';
$this->params['breadcrumbs'][] = ['label' => 'Pelaporans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pelaporan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
