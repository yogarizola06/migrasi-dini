<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PelaporanClass */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pelaporans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pelaporan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Pelaporan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_pelaporan',
            'id_pengungsi',
            'id_community_house',
            'foto_terbaru',
            'periode_lapor',
            //'created_at',
            //'updated_at',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Pelaporan $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id_pelaporan' => $model->id_pelaporan]);
                 }
            ],
        ],
    ]); ?>


</div>
