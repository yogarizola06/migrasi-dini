<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\bootstrap4\Breadcrumbs;
use app\assets\MenuAsset;

MenuAsset::register($this);
$controller = Yii::$app->controller->id;
$action = Yii::$app->controller->action->id;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description"
        content="Chameleon Admin is a modern Bootstrap 4 webapp &amp; admin dashboard html template with a large number of components, elegant design, clean and organized code.">
    <meta name="keywords"
        content="admin template, Chameleon admin template, dashboard template, gradient admin template, responsive admin template, webapp, eCommerce dashboard, analytic dashboard">
    <meta name="author" content="ThemeSelect">
    <title><?= Html::encode($this->title) ?></title>
    <link rel="apple-touch-icon" href="<?= Url::base().'/menu/images/ico/sehat-ri1.png' ?>">
    <link rel="shortcut icon" type="image/x-icon" href="<?= Url::base().'/img/logo_pupr.png' ?>">
    <?php $this->registerCsrfMetaTags() ?>
    <?php $this->head() ?>
</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu 2-columns  fixed-navbar" data-open="click" data-menu="vertical-menu"
    data-color="bg-gradient-y-blue" data-col="2-columns">
    <?php $this->beginBody() ?>
    <!-- BEGIN: Header-->
    <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-light">
        <div class="navbar-wrapper">
            <div class="navbar-container content">
                <div class="collapse navbar-collapse show" id="navbar-mobile">
                    <ul class="nav navbar-nav mr-auto float-left">
                        <li class="nav-item mobile-menu d-md-none mr-auto"><a
                                class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i
                                    class="ft-menu font-large-1"></i></a></li>
                        <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs"
                                href="#"><i class="ft-menu"></i></a></li>
                        <li class="nav-item d-none d-md-block"><a class="nav-link nav-link-expand" href="#"><i
                                    class="ficon ft-maximize"></i></a></li>
                    </ul>
                    <ul class="nav navbar-nav float-right">
                        <li class="dropdown dropdown-user nav-item"><a
                                class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown"
                                style="padding: 1.1rem 0.5rem;">
                                <span class="avatar avatar-online"><img
                                        src="<?=  isset(Yii::$app->user->identity->image_profile) ?  Url::base().'/'.Yii::$app->user->identity->image_profile : Url::base().'/menu/images/portrait/small/avatar-s-19.png' ?>"
                                        alt="avatar"></span></a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <div class="arrow_box_right">
                                    <a class="dropdown-item" href="#"><span class="avatar avatar-online">
                                            <img src="<?=  isset(Yii::$app->user->identity->image_profile) ?  Url::base().'/'.Yii::$app->user->identity->image_profile : Url::base().'/menu/images/portrait/small/avatar-s-19.png' ?>"
                                                alt="avatar"><span
                                                class="user-name text-bold-700 ml-1"><?= Yii::$app->user->identity->username; ?></span></span></a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="<?= Url::to(['user/update-password']) ?>"><i
                                            class="ft-lock"></i> Change Password </a>
                                    <div class="dropdown-divider"></div>
                                    <?= Html::a('<i class="ft-power"></i> '.Yii::t('app', 'Logout'), ['login/logout'], ['class' => 'dropdown-item', 'data-method'=>'post']); ?>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <!-- END: Header-->


    <!-- BEGIN: Main Menu-->
    <div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true"
        data-img="<?= Url::base().'/menu/images/backgrounds/01.jpg' ?>">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mr-auto"><a class="navbar-brand" href="">
                        <h3 class="brand-text">E-CHO</h3><br>
                    </a></li>
                <li class="nav-item d-md-none"><a class="nav-link close-navbar"><i class="ft-x"></i></a></li>
            </ul>
        </div>
        <div class="navigation-background"></div>
        <div class="main-menu-content">
        	<ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
	            <li class="nav-item">
				    <a href="<?= Url::to(['/dashboard/karadenim']) ?>"
				        class="nav-link <?= ($action === "karadenim" && $controller === "dashboard") ? "active" : "" ?>">
				        <i class="ft-home"></i><span>Dashboard</span></a>
				</li>
				<li class="nav-item"><a href="#"><i class="ft-navigation"></i><span class="menu-title" data-i18n="">Data Master</span></a>
				    <ul class="menu-content">
		             	<li class="<?= ($action === "index" && $controller === "deteni") ? "active" : "" ?>">
		              		<a class="menu-item" href="<?= Url::to(['/deteni']) ?>">Deteni</a>
		             	</li>
		             	<li class="<?= ($action === "index" && $controller === "community-house") ? "active" : "" ?>">
		              		<a class="menu-item" href="<?= Url::to(['/community-house']) ?>">Community House</a>
		             	</li>
                         <li class="<?= ($action === "index" && $controller === "pengguna") ? "active" : "" ?>">
		              		<a class="menu-item" href="<?= Url::to(['/pengguna']) ?>">Pengguna</a>
		             	</li>
            		</ul>
				</li>
			</ul>
        </div>
    </div>
    <!-- END: Main Menu-->

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-wrapper-before"></div>
            <div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title"><?= $this->title ?></h3>
                </div>
                <div class="content-header-right col-md-8 col-12">
                    <div class="breadcrumbs-top float-md-right">
                        <div class="breadcrumb-wrapper mr-1">
                            <?php
                        echo Breadcrumbs::widget([
                            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                            'options' => [],
                        ]);
                        ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <?= $content ?>
            </div>
        </div>
    </div>
    <!-- END: Content-->

    <!-- BEGIN: Footer-->
    <footer class="footer footer-light navbar-shadow fixed-bottom">
        <div class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span
                class="float-md-left d-block d-md-inline-block"><?= date('Y') ?> © Copyright <a
                    class="text-bold-800 grey darken-2" href="#" target="_blank">DISKOMINFO KAB KAMPAR</a></span>
            <span class="float-md-right d-block d-md-inline-block">
                <b id="ip"><?= Yii::$app->request->userIP ?></b> | <b id="location"></b> | <b id="time">Last access at
                    -</b>
            </span>
        </div>
    </footer>
    <!-- END: Footer-->
    <?php $this->endBody() ?>
</body>
<!-- END: Body-->
<script>
$(document).ready(function() {
    var zoom = 1;
    $('.zoom').on('click', function() {
        zoom += 0.1;
        $('body').css('zoom', zoom);
    });
    $('.zoom-init').on('click', function() {
        zoom = 1;
        $('body').css('zoom', zoom);
    });
    $('.zoom-out').on('click', function() {
        zoom -= 0.1;
        $('body').css('zoom', zoom);
    });
});
</script>

</html>
<?php $this->endPage() ?>