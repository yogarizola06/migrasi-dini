<?php

/* @var $this \yii\web\View */
/* @var $content string */
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use richardfan\widget\JSRegister;
use yii\bootstrap4\Modal;
use yii\bootstrap4\Breadcrumbs;
use yii\widgets\Pjax;
$this->title = 'E-CHO';

?>

<div class="classes-index">
    <div class="row">
        <div class="col-xl-12 col-lg-12">
            <div class="row match-height">
                <div class="col-xl-4 col-lg-6 col-md-12">
                    <div class="card" style="height: 437px;">
                        <div class="card-header ">
                            <a class="heading-elements-toggle">
                                <i class="la la-ellipsis-v font-medium-3"></i>
                            </a>
                        </div>
                        <div class="card-content">
                            <div class="card-body text-center">
                                <div class="card-header pt-0 pb-0">
                                    <p class="success darken-2">Total Migrasi</p>
                                    <h3 class="display-4 blue-grey lighten-1"></h3>
                                </div>
                                <div class="card-content">
                                    <br>
                                    <i class="icon-envelope-letter icon-opacity success font-large-5"></i>
                                    <br>
                                    <hr>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-6 col-md-12">
                    <div class="card" style="height: 437px;">
                        <div class="card-header ">
                            <a class="heading-elements-toggle">
                                <i class="la la-ellipsis-v font-medium-3"></i>
                            </a>
                        </div>
                        <div class="card-content">
                            <div class="card-body text-center">
                                <div class="card-header pt-0 pb-0">
                                    <p class="info darken-2">Total Post</p>
                                    <h3 class="display-4 blue-grey lighten-1"></h3>
                                </div>
                                <div class="card-content">
                                    <br>
                                    <i class="icon-directions icon-opacity info font-large-5"></i>
                                    <br>
                                    <hr>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-12">
                    <div class="card" style="height: 437px;">
                        <div class="card-header ">
                            <a class="heading-elements-toggle">
                                <i class="la la-ellipsis-v font-medium-3"></i>
                            </a>
                        </div>
                        <div class="card-content">
                            <div class="card-body text-center">
                                <div class="card-header pt-0 pb-0">
                                    <p class="danger darken-2">Total</p>
                                    <h3 class="display-4 blue-grey lighten-1"></h3>
                                </div>
                                <div class="card-content">
                                    <br>
                                    <i class="icon-ban icon-opacity danger font-large-5"></i>
                                    <br>
                                    <hr>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$this->registerJsFile(Url::base()."/menu/js/scripts/cards/card-advanced.min.js");
?>