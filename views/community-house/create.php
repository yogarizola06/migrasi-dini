<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\deteni */

$this->title = 'Tambah Community House';
$this->params['breadcrumbs'][] = ['label' => 'Community House', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="community-house-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
