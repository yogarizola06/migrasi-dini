<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CommunityHouse */

$this->title = $model->id_community_house;
$this->params['breadcrumbs'][] = ['label' => 'Community Houses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="community-house-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id_community_house' => $model->id_community_house], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id_community_house' => $model->id_community_house], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_community_house',
            'nama_community_house',
        ],
    ]) ?>

</div>
