<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap4\Modal;
use richardfan\widget\JSRegister;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CommunityHouseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Community Houses';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="community-house-index">
    <div class="row">
        <div class="col-xl-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h3>Deteni
                    <a href="<?= Url::to(['create']) ?>" class="pull-right btn btn-sm btn-success">Tambah Data Community House</a></h3>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12 table-responsive">
                            <?= GridView::widget([
                                'dataProvider' => $dataProvider,
                                'filterModel' => $searchModel,
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],
                                    'nama_community_house',
                                    [
                                        'class' => 'yii\grid\ActionColumn',
                                        'template' => '<div class="btn-group">{update}{remove}</div>',
                                        'buttons' => [
                                            'update' => function ($url,$model) {
                                                return Html::a(
                                                    '<span class="fa fa-edit"></span>' ,Url::to(['update', 'id_community_house' => $model->id_community_house ]), [
                                                        'class'=>'btn btn-sm btn-warning',
                                                        'data-id'=>$model->id_community_house ,
                                                        'data-toggle'=>'tooltip',
                                                        'data-placement'=>'left',
                                                        'title'=>'Klik disini untuk edit deteni',
                                                    ]);
                                            },
                                            'remove' => function ($url,$model) {
                                                return Html::button(Yii::t('app', '<span class="fa fa-trash"></span>'),
                                                  [
                                                    'class' => 'btn btn-sm btn-danger remove_deteni',
                                                    'data-toggle'=>'tooltip',
                                                    'data-id' => $model->id_community_house ,
                                                    'data-placement'=>'top', 
                                                    'title'=>'Klik disini untuk hapus community house',
                                                ]);
                                            },
                                        ],
                                    ],
                                ],
                            ]); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php
Modal::begin([
    'id'=>'modal',
    'title'=>'',
    'options' => ['tabindex' => false],
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => false],
    'size'=>'modal-lg',
]);
echo "<div id='modalContent'></div>";
Modal::end();
?>
<?php JSRegister::begin() ?>
<script>

    $(".remove_deteni").click(function(e) {
        remove($(this).data("id"))
    });

    function remove(id) {
        swal({
            title: "Konfirmasi Hapus!",
            text: "Apakah anda yakin ingin menghapus data community house ?",
            icon: "warning",
            buttons: true,
            showCancelButton: true,
            dangerMode: true,

        }).then((res) => {
            console.log(res);
            if (res.value) {
                $.ajax({
                    url: "<?= Url::to(['remove']) ?>?id_community_house=" + id,
                    type: "POST",
                    contentType: false,
                    processData: false,
                    success: function(data_res) {
                        var data = JSON.parse(data_res);
                        swal({
                            title: data.title,
                            text: data.message,
                            icon: data.type
                        }).then((result) => {
                            location.href = "<?= Url::to(['index']) ?>";
                        });
                    }
                })

            } else {
                swal("Batal", "Aksi dibatalkan", "success");
            }
        });
    }

</script>
<?php JSRegister::end() ?>