<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CommunityHouse */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="community-house-form">

<section id="base-style">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h1>Form Community House</h1>
                </div>

                <?php if(Yii::$app->session->hasFlash('success')): ?>
                    <div class="alert alert-success" role="alert">
                        <?= Yii::$app->session->getFlash('success'); ?>
                    </div>
                <?php endif; ?>

                <?php if(Yii::$app->session->hasFlash('danger')): ?>
                    <div class="alert alert-danger" role="alert">
                        <?= Yii::$app->session->getFlash('danger'); ?>
                    </div>
                <?php endif; ?>

                <div class="card-content collapse show">
                    <div class="card-body card-dashboard">
                        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

                        <?= $form->field($model, 'nama_community_house')->textInput(['maxlength' => true]) ?>

                        <div class="form-actions right">
                            <a class="btn  btn-danger "href="<?= Url::to(['index']) ?>">Kembali</a>
                            <?= Html::submitButton(Yii::t('app', 'Simpan'), ['class' => 'btn btn-success']) ?>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>


</div>
