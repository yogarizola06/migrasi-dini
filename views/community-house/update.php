<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\community-house */

$this->title = 'Ubah community house: ' . $model->nama_community_house;
$this->params['breadcrumbs'][] = ['label' => 'Community House', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama_community_house, 'url' => ['view', 'id_community_house' => $model->id_community_house]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="community-house-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
