<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii2mod\alert\Alert;
use richardfan\widget\JSRegister;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <section class="flexbox-container">
        <div class="col-12 d-flex align-items-center justify-content-center">
            <div class="col-lg-4 col-md-6 col-10 box-shadow-2 p-0">
                <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                    <div class="card-header border-0">
                        <div class="text-center mb-1">
                            <img src="<?= Url::base().'/auth/images/images/rubah.png' ?>" alt="branding logo"
                                style="width:200px">
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <div id="div-login">
                                <?php $form = ActiveForm::begin(['id' => 'login-form', "class"=>"form-horizontal"]); ?>
                                <input type="hidden" value="login" name="LoginForm[action]">
                                <fieldset class=" position-relative has-icon-left">
                                    <?= $form->field($model, 'username', ['errorOptions'=>['class'=>'help-block help-block-error']])->textInput(['autocomplete'=>'off', 'autofocus' => true,'placeholder'=>'Masukkan username', "class"=>"form-control "])->label(false) ?>
                                    <div class="form-control-position">
                                        <i class="ft-user"></i>
                                    </div>
                                </fieldset>
                                <fieldset class="form-group position-relative has-icon-left">
                                    <?= $form->field($model, 'password')->passwordInput(['autocomplete'=>'cc-csc','placeholder'=>'Input Password', "class"=>"form-control" ])->label(false) ?>
                                    <div class="form-control-position">
                                        <i class="ft-lock"></i>
                                    </div>
                                </fieldset>
                                <div class="form-group text-center">
                                    <?= Html::submitButton('<i class="fa fa-key"></i> Login', ['class' => 'btn round btn-block btn-glow btn-bg-gradient-x-red-pink col-12 mr-1 mb-1','id'=>'login']) ?>
                                </div>
                                <?php ActiveForm::end(); ?>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-3 col-12">
                                    <p class="card-subtitle text-left font-small-3 mt-2">
                                        <?= Yii::$app->request->userIP;?>
                                    </p>
                                </div>
                                <div class="col-md-5 col-12">
                                    <p class="card-subtitle text-left font-small-3 mt-2" id="location">
                                    </p>
                                </div>
                                <div class="col-md-4 col-12">
                                    <p class="card-subtitle text-left font-small-3 mt-2" id="time">
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>