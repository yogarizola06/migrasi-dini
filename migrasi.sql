-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 23, 2022 at 08:27 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `migrasi`
--

-- --------------------------------------------------------

--
-- Table structure for table `community_house`
--

CREATE TABLE `community_house` (
  `id_community_house` int(5) NOT NULL,
  `nama_community_house` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `deteni`
--

CREATE TABLE `deteni` (
  `id_deteni` int(5) NOT NULL,
  `nama_deteni` varchar(200) NOT NULL,
  `jk` varchar(1) NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `kewarganegaraan` varchar(100) NOT NULL,
  `no_unhc` varchar(20) NOT NULL,
  `tgl_masuk` date NOT NULL,
  `foto` varchar(100) NOT NULL,
  `ket` varchar(2) NOT NULL,
  `id_community_house` int(5) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pelaporan`
--

CREATE TABLE `pelaporan` (
  `id_pelaporan` int(5) NOT NULL,
  `id_pengungsi` int(5) NOT NULL,
  `id_community_house` int(5) NOT NULL,
  `foto_terbaru` varchar(100) NOT NULL,
  `periode_lapor` date NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE `pengguna` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `community_house_id` int(5) NOT NULL,
  `auth_key` varchar(255) NOT NULL,
  `access_token` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`id`, `username`, `password`, `community_house_id`, `auth_key`, `access_token`) VALUES
(1, 'admin_ch', '$2y$13$wq.tpX17neY65bs6zwt4YuQN/c3dcnLRkmxUa8H9LPntt4ziSOsFa', 1, '', 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MzIsIm5hbWUiOiJ5b2dhcml6b2xhMDYxIiwicGhvbmVfbnVtYmVyIjpudWxsLCJlbWFpbCI6InlvZ2FyaXpvbGEwNkBnbWFpMjFsMi5jb20iLCJ1c2VybmFtZSI6InlvZ2FyaXpvbGEwNiIsInBhc3N3b3JkIjoiJDJ5JDEzJEl4MkhBWHZkaVBVOUZUbmtnUmJGNi5rRWVYem82Q1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `community_house`
--
ALTER TABLE `community_house`
  ADD PRIMARY KEY (`id_community_house`);

--
-- Indexes for table `deteni`
--
ALTER TABLE `deteni`
  ADD PRIMARY KEY (`id_deteni`);

--
-- Indexes for table `pelaporan`
--
ALTER TABLE `pelaporan`
  ADD PRIMARY KEY (`id_pelaporan`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `community_house`
--
ALTER TABLE `community_house`
  MODIFY `id_community_house` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `deteni`
--
ALTER TABLE `deteni`
  MODIFY `id_deteni` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pelaporan`
--
ALTER TABLE `pelaporan`
  MODIFY `id_pelaporan` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pengguna`
--
ALTER TABLE `pengguna`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
