<?php

return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@protected' => '/protected'
    ],
    'asset' => [
        'bundles' => [
            'yii\bootstrap4\BootstrapAsset' => [
                'sourcePath' => '@npm/bootstrap/dist'
            ],
            'yii\bootstrap4\BootstrapPluginAsset' => [
                'sourcePath' => '@npm/bootstrap/dist'
            ],
            'yii\bootstrap4\BootstrapPluginAsset' => [
                'sourcePath' => '@npm/bootstrap/dist'
            ],
            'yii\bootstrap\BootstrapPluginAsset' => [
                'js'=>[]
            ],
            'yii\bootstrap\BootstrapAsset' => [
                'css' => [],
            ],
            'yii\bootstrap\BootstrapPluginAsset' => [
                'js'=>[]
            ],
        ]
    ]
];
