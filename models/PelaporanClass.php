<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Pelaporan;

/**
 * PelaporanClass represents the model behind the search form of `app\models\Pelaporan`.
 */
class PelaporanClass extends Pelaporan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_pelaporan', 'id_pengungsi', 'id_community_house'], 'integer'],
            [['foto_terbaru', 'periode_lapor', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pelaporan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_pelaporan' => $this->id_pelaporan,
            'id_pengungsi' => $this->id_pengungsi,
            'id_community_house' => $this->id_community_house,
            'periode_lapor' => $this->periode_lapor,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'foto_terbaru', $this->foto_terbaru]);

        return $dataProvider;
    }
}
