<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Deteni;

/**
 * deteniSearch represents the model behind the search form of `app\models\deteni`.
 */
class DeteniSearch extends Deteni
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_deteni', 'id_community_house'], 'integer'],
            [['nama_deteni', 'jk', 'tempat_lahir', 'tgl_lahir', 'kewarganegaraan', 'no_unhc', 'tgl_masuk', 'foto', 'ket', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = deteni::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_deteni' => $this->id_deteni,
            'tgl_lahir' => $this->tgl_lahir,
            'tgl_masuk' => $this->tgl_masuk,
            'id_community_house' => $this->id_community_house,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'nama_deteni', $this->nama_deteni])
            ->andFilterWhere(['like', 'jk', $this->jk])
            ->andFilterWhere(['like', 'tempat_lahir', $this->tempat_lahir])
            ->andFilterWhere(['like', 'kewarganegaraan', $this->kewarganegaraan])
            ->andFilterWhere(['like', 'no_unhc', $this->no_unhc])
            ->andFilterWhere(['like', 'foto', $this->foto])
            ->andFilterWhere(['like', 'ket', $this->ket]);

        return $dataProvider;
    }
}
