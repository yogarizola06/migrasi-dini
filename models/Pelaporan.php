<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pelaporan".
 *
 * @property int $id_pelaporan
 * @property int $id_pengungsi
 * @property int $id_community_house
 * @property string $foto_terbaru
 * @property string $periode_lapor
 * @property string $created_at
 * @property string $updated_at
 */
class Pelaporan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pelaporan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_pengungsi', 'id_community_house', 'foto_terbaru', 'periode_lapor', 'created_at', 'updated_at'], 'required'],
            [['id_pengungsi', 'id_community_house'], 'integer'],
            [['periode_lapor', 'created_at', 'updated_at'], 'safe'],
            [['foto_terbaru'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_pelaporan' => 'Id Pelaporan',
            'id_pengungsi' => 'Id Pengungsi',
            'id_community_house' => 'Id Community House',
            'foto_terbaru' => 'Foto Terbaru',
            'periode_lapor' => 'Periode Lapor',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
