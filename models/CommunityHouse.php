<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "community_house".
 *
 * @property int $id_community_house
 * @property string $nama_community_house
 */
class CommunityHouse extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'community_house';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_community_house'], 'required'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['nama_community_house'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_community_house' => 'Id Community House',
            'nama_community_house' => 'Nama Community House',
        ];
    }
}
