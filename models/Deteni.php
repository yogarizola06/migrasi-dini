<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dateni".
 *
 * @property int $id_dateni
 * @property string $nama_dateni
 * @property string $jk
 * @property string $tempat_lahir
 * @property string $tgl_lahir
 * @property string $kewarganegaraan
 * @property string $no_unhc
 * @property string $tgl_masuk
 * @property string $foto
 * @property string $ket
 * @property int $id_community_house
 * @property string $created_at
 * @property string $updated_at
 */
class Deteni extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'deteni';
    }

    /**
     * {@inheritdoc}
     */

    public $tmp_image;


    public function rules()
    {
        return [
            [['nama_deteni', 'jk', 'tempat_lahir', 'tgl_lahir', 'kewarganegaraan', 'no_unhc', 'tgl_masuk', 'foto', 'ket', 'id_community_house', 'created_at', 'updated_at', 'deleted_at'], 'required'],
            [['tgl_lahir', 'tgl_masuk', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id_community_house'], 'integer'],
            [['nama_deteni'], 'string', 'max' => 200],
            [['jk'], 'string', 'max' => 1],
            [['tempat_lahir', 'kewarganegaraan', 'foto'], 'string', 'max' => 100],
            [['no_unhc'], 'string', 'max' => 20],
            [['ket'], 'string', 'max' => 2],
            [['tmp_image'],'file','skipOnEmpty'=>true, 'extensions' => ['png', 'jpg', 'doc', 'pdf'], 'maxSize' => 1024 * 2000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_deteni' => 'Id Deteni',
            'nama_deteni' => 'Nama Deteni',
            'jk' => 'Jenis Kelamin',
            'tempat_lahir' => 'Tempat Lahir',
            'tgl_lahir' => 'Tgl Lahir',
            'kewarganegaraan' => 'Kewarganegaraan',
            'no_unhc' => 'No UNHC',
            'tgl_masuk' => 'Tgl Masuk',
            'foto' => 'Foto',
            'ket' => 'Ket',
            'tmp_image' => 'Masukkan Foto Terbaru',
            'id_community_house' => 'Id Community House',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
